import SwiftUI

struct ContentView: View {
    
    @State private var showAddPostView: Bool = false
    
    let viewModel = PostViewModel(
        repository: PostRepository(
            postService: PostService(),
            postOfflineService: PostsOfflineService(persistentContainer: PersistenceController.shared.container)
        )
    )
    
    var body: some View {
        NavigationView {
            PostListView(viewModel: viewModel)
                .navigationBarTitle("Feeds")
                .toolbar {
                    Button("Add Post") {
                        showAddPostView.toggle()
                    }
                }
        }
        .sheet(isPresented: $showAddPostView, content: {
            AddPostView(viewModel: viewModel)
        })
    }
}

#Preview {
    ContentView()
}
