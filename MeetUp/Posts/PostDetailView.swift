import SwiftUI

struct PostDetailView: View {
    @ObservedObject var viewModel: PostViewModel
    @State var post: Post
    
    var body: some View {
        ScrollView {
            HStack(spacing: 0) {
                VStack(alignment: .leading, spacing: 0) {
                    Text(post.title)
                        .font(.title3)
                        .fontWeight(.medium)
                        .padding(.bottom, 5)
                    
                    Text(post.body)
                        .font(.subheadline)
                        .foregroundColor(.gray)
                    
                    HStack(spacing: 0) {
                        Spacer()
                        Button(action: {
                            // TODO:
                        }, label: {
                            Image(systemName: post.liked ? "hand.thumbsup.fill" : "hand.thumbsup")
                                .resizable()
                                .frame(width: 30, height: 30)
                                .foregroundColor(.blue)
                        })
                    }
                    .padding(10)
                }
                .padding(.horizontal, 16)
                .padding(.vertical, 10)
            }
            
            Divider()
            
            ForEach(viewModel.selectedPostComments) { comment in
                HStack(spacing: 0) {
                    VStack(alignment: .trailing, spacing: 0) {
                        Text(comment.body)
                            .font(.body)
                            .foregroundColor(.gray)
                        
                        Text("By: \(comment.name)")
                            .font(.caption2)
                            .foregroundColor(.gray)
                    }
                    .padding(.leading, 10)
                    
                    Spacer()
                }
                .padding(5)
            }
            
            VStack(alignment: .leading) {
                Text("Add Comment:")
                TextEditor(text: $viewModel.newComment)
                    .frame(height: 50)
                    .border(.gray)
                
                Button {
                    viewModel.addComment(post: post) {
                        // Dismiss Keyboard
                        DispatchQueue.main.async {
                            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                        }
                    }
                } label: {
                    Text("Submit")
                        .bold()
                        .frame(height: 50)
                        .frame(maxWidth: .infinity)
                        .background(
                            RoundedRectangle(cornerRadius: 10, style: .continuous)
                                .fill(.gray)
                        )
                        .foregroundColor(.white)
                }
            }
            .padding()
        }
        .navigationBarTitleDisplayMode(.inline)
        .onAppear {
            viewModel.fetchComments(postId: post.id)
        }
    }
}
