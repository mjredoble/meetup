import Combine
import Foundation

protocol PostRepositoryProtocol {
    func getPosts() -> AnyPublisher<[Post], Error>
    func getComments() -> AnyPublisher<[Comment], Error>
    func addPost(post: String, callback: @escaping(Post) -> ())
    func getComments(postId: Int) -> AnyPublisher<[Comment], Error>
    func addComment(postId: Int, comment: String, callback: @escaping(Comment) -> ())
}

class PostRepository: PostRepositoryProtocol {
    let postService: PostsServiceProtocol
    let postOfflineService: PostsOfflineServiceProtocol
    
    init(postService: PostsServiceProtocol, postOfflineService: PostsOfflineServiceProtocol) {
        self.postService = postService
        self.postOfflineService = postOfflineService
    }
    
    func addPost(post: String, callback: @escaping(Post) -> ()) {
        postService.addPost(post: post, callback: { post in
            self.postOfflineService.save(post: post)
            callback(post)
        })
    }
    
    func addComment(postId: Int, comment: String, callback: @escaping(Comment) -> ()) {
        postService.addComment(postId: postId, comment: comment, callback: { comment in
            self.postOfflineService.save(comment: comment)
            callback(comment)
        })
    }
    
    func getPosts() -> AnyPublisher<[Post], Error> {
        postService.getPosts()
            .flatMap { posts in
                self.postOfflineService.save(posts: posts)
                return self.postOfflineService.getPosts()
            }
            .catch { error -> AnyPublisher<[Post], Error> in
                print("Failed to fetch todos from service: \(error)")
                return self.postOfflineService.getPosts()
            }
            .eraseToAnyPublisher()
    }
    
    func getComments() -> AnyPublisher<[Comment], Error> {
        postService.getComments()
            .map { [weak self] comments in
                self?.postOfflineService.save(comments: comments)
                return comments
            }
            .catch { error -> AnyPublisher<[Comment], Error> in
                print("Failed to fetch todos from service: \(error)")
                return self.postOfflineService.getComments()
            }
            .eraseToAnyPublisher()
    }
    
    func getComments(postId: Int) -> AnyPublisher<[Comment], Error> {
        postOfflineService.getComments(postId: postId)
            .map { comments in
                return comments
            }
            .catch { error -> AnyPublisher<[Comment], Error> in
                print("Failed to fetch todos from service: \(error)")
                return self.postOfflineService.getComments()
            }
            .eraseToAnyPublisher()
    }
}
