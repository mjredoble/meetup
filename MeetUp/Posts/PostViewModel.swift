import Combine
import Foundation
import SwiftUI


class PostViewModel: ObservableObject {
    let repository: PostRepositoryProtocol
    
    var cancellables = Set<AnyCancellable>()
    
    @Published var posts: [Post] = []
    @Published var allComments: [Comment] = []
    @Published var newPost: String = ""
    @Published var newComment: String = ""
    @Published var selectedPostComments: [Comment] = []
    @Published var isLoading: Bool = false
    
    init(repository: PostRepositoryProtocol) {
        self.repository = repository
    }
    
    func addPost(callback: @escaping () -> ()) {
        repository.addPost(post: newPost, callback: { post in
            self.newPost = ""
            DispatchQueue.main.async {
                self.posts.append(post)
                callback()
            }
        })
    }
    
    func addComment(post: Post, callback: @escaping () -> ()) {
        repository.addComment(postId: post.id, comment: newComment, callback: { comment in
            self.newComment = ""
            DispatchQueue.main.async {
                self.selectedPostComments.append(comment)
                callback()
            }
        })
    }
    
    func fetchPostAndComments() {
        fetchPosts()
        fetchComments()
    }
    
    func fetchComments(postId: Int) {
        repository.getComments(postId: postId)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("Failed to fetch todos: \(error)")
                }
            } receiveValue: { [weak self] results in
                self?.selectedPostComments.removeAll()
                self?.selectedPostComments.append(contentsOf: results)
            }.store(in: &cancellables)
    }
    
    private func fetchPosts() {
        repository.getPosts()
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("Failed to fetch todos: \(error)")
                }
            } receiveValue: { [weak self] results in
                DispatchQueue.main.async {
                    self?.posts = results
                }
            }.store(in: &cancellables)
    }
    
    private func fetchComments() {
        repository.getComments()
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("Failed to fetch todos: \(error)")
                }
            } receiveValue: { [weak self] results in
                DispatchQueue.main.async {
                    self?.allComments = results
                }
            }.store(in: &cancellables)
    }
}
