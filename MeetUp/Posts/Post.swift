import Foundation

struct Post: Codable, Identifiable {
    let id: Int
    let userId: Int
    let title: String
    let body: String
    let liked: Bool
    
    init(entity: PostEntity) {
        id = Int(entity.id)
        userId = Int(entity.userId)
        title = entity.title ?? "-"
        body = entity.body ?? "-"
        liked = entity.liked
    }
    
    init(id: Int, userId: Int, title: String, body: String, liked: Bool) {
        self.id = id
        self.userId = userId
        self.title = title
        self.body = body
        self.liked = liked
    }
}
