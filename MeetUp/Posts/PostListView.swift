import SwiftUI

struct PostListView: View {
    @ObservedObject var viewModel: PostViewModel
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 0) {
                ForEach(viewModel.posts) { post in
                    NavigationLink(destination: PostDetailView(viewModel: viewModel, post: post), label: {
                        HStack(spacing: 0) {
                            VStack(alignment: .leading, spacing: 0) {
                                Text(post.title)
                                    .font(.headline)
                                    .padding(.bottom, 5)
                                
                                Text(post.body)
                                    .font(.subheadline)
                                    .foregroundColor(.gray)
                                
                                HStack {
                                    Spacer()
                                    Image(systemName: post.liked ? "hand.thumbsup.fill" : "hand.thumbsup")
                                }
                                .padding(5)
                            }
                            .padding(.horizontal, 16)
                            .padding(.vertical, 8)
                        }
                    })
                    
                    Divider()
                }
            }
        }
        .onAppear {
            viewModel.fetchPostAndComments()
        }
    }
}

