import SwiftUI

struct AddPostView: View {
    @ObservedObject var viewModel: PostViewModel
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                Text("What is happening?!")
                TextEditor(text: $viewModel.newPost)
                    .frame(height: 100)
                    .border(.gray)
                
                Button {
                    viewModel.addPost {
                        self.presentationMode.wrappedValue.dismiss()
                        viewModel.fetchPostAndComments()
                        
                        // Dismiss Keyboard
                        DispatchQueue.main.async {
                            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                        }
                    }
                } label: {
                    Text("Submit")
                        .bold()
                        .frame(height: 50)
                        .frame(maxWidth: .infinity)
                        .background(
                            RoundedRectangle(cornerRadius: 10, style: .continuous)
                                .fill(.gray)
                        )
                        .foregroundColor(.white)
                }
                
                Spacer()
            }
            .padding([.top, .horizontal])
            .navigationBarTitle("Add New Post")
            .navigationBarTitleDisplayMode(.inline)
            .navigationBarItems(trailing: Button("Cancel") {
                self.presentationMode.wrappedValue.dismiss()
            })
        }
    }
}
