import Combine
import Foundation

protocol PostsServiceProtocol {
    func getPosts() -> AnyPublisher<[Post], Error>
    func getComments() -> AnyPublisher<[Comment], Error>
    func addPost(post: String, callback: @escaping(Post) -> ())
    func addComment(postId: Int, comment: String, callback: @escaping(Comment) -> ())
}

class PostService: PostsServiceProtocol {
    let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func getPosts() -> AnyPublisher<[Post], Error> {
        let postURL = URL(string: "https://my-json-server.typicode.com/mjredoble/ideal-garbanzo/posts")!
        return session.dataTaskPublisher(for: postURL)
            .map(\.data)
            .decode(type: [Post].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    func getComments() -> AnyPublisher<[Comment], Error> {
        let commentURL = URL(string: "https://my-json-server.typicode.com/mjredoble/ideal-garbanzo/comments")!
        return session.dataTaskPublisher(for: commentURL)
            .map(\.data)
            .decode(type: [Comment].self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    func addPost(post: String, callback: @escaping(Post) -> ()) {
        let postURL = URL(string: "https://my-json-server.typicode.com/mjredoble/ideal-garbanzo/posts")!
        
        let payload = [
            "userId": "1",
            "body": post,
            "title": "New Post"
        ]
        
        let postData = (try? JSONSerialization.data(withJSONObject: payload, options: []))
        var request = URLRequest(url: postURL)
        request.timeoutInterval = TimeInterval(0)
        request.httpMethod = HTTPMethod.POST.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = postData
        
        Task {
            if let result = try? await APIClient().request(url: postURL, method: .POST) {
                let resultDTO = try JSONDecoder().decode(Result.self, from: result.body)
                callback(Post(id: resultDTO.id, userId: 1, title: "New Post", body: post, liked: false))
            }
        }
    }
    
    func addComment(postId: Int, comment: String, callback: @escaping(Comment) -> ()) {
        let postURL = URL(string: "https://my-json-server.typicode.com/mjredoble/ideal-garbanzo/comments")!
        
        let payload = [
            "postId": postId,
            "body": comment,
            "name": "Me"
        ] as [String : Any]
        
        let postData = (try? JSONSerialization.data(withJSONObject: payload, options: []))
        var request = URLRequest(url: postURL)
        request.timeoutInterval = TimeInterval(0)
        request.httpMethod = HTTPMethod.POST.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = postData
        
        Task {
            if let result = try? await APIClient().request(url: postURL, method: .POST) {
                let resultDTO = try JSONDecoder().decode(Result.self, from: result.body)
                DispatchQueue.main.async {
                    callback(Comment(id: resultDTO.id, postId: postId, name: "Me", body: comment))
                }
            }
        }
    }
}


struct Result: Codable {
    let id: Int
}
