import Combine
import CoreData
import Foundation

protocol PostsOfflineServiceProtocol {
    func getPosts() -> AnyPublisher<[Post], Error>
    func save(posts: [Post])
    func save(post: Post)
    func getComments() -> AnyPublisher<[Comment], Error>
    func save(comments: [Comment])
    func save(comment: Comment) 
    func getComments(postId: Int) -> AnyPublisher<[Comment], Error>
}

class PostsOfflineService: PostsOfflineServiceProtocol {
    let persistentContainer: NSPersistentContainer
    
    init(persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
    }
    
    func getPosts() -> AnyPublisher<[Post], Error> {
        let request: NSFetchRequest<PostEntity> = PostEntity.fetchRequest()
        let context = persistentContainer.viewContext
     
        return Future<[Post], Error> { promise in
            do {
                let postEntries = try context.fetch(request)
                let posts = postEntries.map { Post(entity: $0) }
                promise(.success(posts))
            } catch {
                promise(.failure(error))
            }
        }
        .eraseToAnyPublisher()
    }
    
    func save(posts: [Post]) {
        let context = persistentContainer.newBackgroundContext()
        context.performAndWait {
            do {
                let request: NSFetchRequest<PostEntity> = PostEntity.fetchRequest()
                let existingPostsEntities = try context.fetch(request)
                let existingPostsIds = existingPostsEntities.compactMap { Int($0.id) }
                
                posts.forEach { post in
                    if !existingPostsIds.contains(post.id) {
                        let postEntity = PostEntity(context: context)
                        postEntity.id = Int64(post.id)
                        postEntity.userId = Int64(post.userId)
                        postEntity.title = post.title
                        postEntity.body = post.body
                    }
                }
                
                try context.save()
            } catch {
                print("Failed to save posts to core data: \(error)")
            }
        }
    }
    
    func save(post: Post) {
        let context = persistentContainer.newBackgroundContext()
        context.performAndWait {
            do {
                let request: NSFetchRequest<PostEntity> = PostEntity.fetchRequest()
                let allElementsCount = try context.count(for: request)
                let existingPostsEntities = try context.fetch(request)
                let existingPostsIds = existingPostsEntities.compactMap { Int($0.id) }
                
                let newId = allElementsCount + 1
                if !existingPostsIds.contains(newId) {
                    let postEntity = PostEntity(context: context)
                    postEntity.id = Int64(newId)
                    postEntity.userId = Int64(post.userId)
                    postEntity.title = post.title
                    postEntity.body = post.body
                }
                
                try context.save()
            } catch {
                print("Failed to save posts to core data: \(error)")
            }
        }
    }
    
    func getComments() -> AnyPublisher<[Comment], Error> {
        let request: NSFetchRequest<CommentEntity> = CommentEntity.fetchRequest()
        let context = persistentContainer.viewContext
     
        return Future<[Comment], Error> { promise in
            do {
                let commentEntries = try context.fetch(request)
                let comments = commentEntries.map { Comment(entity: $0) }
                promise(.success(comments))
            } catch {
                promise(.failure(error))
            }
        }
        .eraseToAnyPublisher()
    }
    
    func getComments(postId: Int) -> AnyPublisher<[Comment], Error> {
        let request: NSFetchRequest<CommentEntity> = CommentEntity.fetchRequest()
        request.predicate = NSPredicate(format: "postId == \(postId)")
        let context = persistentContainer.viewContext
     
        return Future<[Comment], Error> { promise in
            do {
                let entries = try context.fetch(request)
                let comments = entries.map { Comment(entity: $0) }
                promise(.success(comments))
            } catch {
                promise(.failure(error))
            }
        }
        .eraseToAnyPublisher()
    }
    
    func save(comments: [Comment]) {
        let context = persistentContainer.newBackgroundContext()
        context.performAndWait {
            do {
                let request: NSFetchRequest<CommentEntity> = CommentEntity.fetchRequest()
                let existingCommentsEntities = try context.fetch(request)
                let existingCommentIds = existingCommentsEntities.compactMap { Int($0.id) }
                
                comments.forEach { comment in
                    if !existingCommentIds.contains(comment.id) {
                        let commentEntity = CommentEntity(context: context)
                        commentEntity.id = Int64(comment.id)
                        commentEntity.postId = Int64(comment.postId)
                        commentEntity.body = comment.body
                        commentEntity.name = comment.name
                    }
                }
                
                try context.save()
            } catch {
                print("Failed to save comments to core data: \(error)")
            }
        }
    }
    
    func save(comment: Comment) {
        let context = persistentContainer.newBackgroundContext()
        context.performAndWait {
            do {
                let request: NSFetchRequest<CommentEntity> = CommentEntity.fetchRequest()
                let allElementsCount = try context.count(for: request)
                let existingCommentsEntities = try context.fetch(request)
                let existingCommentIds = existingCommentsEntities.compactMap { Int($0.id) }
                
                let newId = allElementsCount + 1
                if !existingCommentIds.contains(newId) {
                    let commentEntity = CommentEntity(context: context)
                    commentEntity.id = Int64(comment.id)
                    commentEntity.postId = Int64(comment.postId)
                    commentEntity.body = comment.body
                    commentEntity.name = comment.name
                }
                
                try context.save()
            } catch {
                print("Failed to save comments to core data: \(error)")
            }
        }
    }
}
