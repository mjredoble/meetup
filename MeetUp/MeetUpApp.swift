import SwiftUI

@main
struct MeetUpApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
