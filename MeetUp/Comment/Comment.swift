import Foundation

struct Comment: Codable, Identifiable {
    let id: Int
    let postId: Int
    let body: String
    let name: String
    
    init(entity: CommentEntity) {
        id = Int(entity.id)
        postId = Int(entity.postId)
        body = entity.body ?? "-"
        name = entity.name ?? "-"
    }
    
    init(id: Int, postId: Int, name: String, body: String) {
        self.id = id
        self.postId = postId
        self.body = body
        self.name = name
    }
}
