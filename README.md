# MeetUp App

MeetUpApp is a simple social media application using mock api from My JSON Server (beta) that fetches posts and comments. This application uses SwiftUI with MVVM + Repository design pattern.

## Library Used
1. SwiftUI
2. Core Data
3. Combine

## Test
1. Not Implemented

## Caching Mechanism Used
1. Core Data

## API Used
1.  My JSON Server (beta)


## What are implemented?
1. Create mock json data on Posts, Comments using My-Json-Server (https://my-json-server.typicode.com/mjredoble/ideal-garbanzo/)
2. Fetch data from API
3. Cache data from API to Core Data
4. Add Post
5. Add Comments

## If given enough time, Things I want to fix/improve
1. Synchronization of local and api data with FastAPI backend
2. Likes
3. Delete your comments
4. Better UI
